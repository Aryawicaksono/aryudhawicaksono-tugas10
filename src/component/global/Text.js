import React from 'react';
import {Text, StyleSheet} from 'react-native';

export const TextRegular = ({text, numberOfLines, size = 14, color, style}) => {
  return (
    <Text
      style={[styles.regulartext, {fontSize: size, color: color}, style]}
      numberOfLines={numberOfLines}>
      {text}
    </Text>
  );
};

export const TextMedium = ({text, numberOfLines, size = 14, color, style}) => {
  return (
    <Text
      style={[styles.semiBoldtext, {fontSize: size, color: color}, style]}
      numberOfLines={numberOfLines}>
      {text}
    </Text>
  );
};

export const TextBold = ({text, numberOfLines, size = 14, color, style}) => {
  return (
    <Text
      style={[styles.boldtext, {fontSize: size, color: color}, style]}
      numberOfLines={numberOfLines}>
      {text}
    </Text>
  );
};

const styles = StyleSheet.create({
  regulartext: {
    color: '#000',
  },
  semiBoldtext: {
    color: '000',
    fontWeight: '500',
  },
  boldtext: {
    color: '#000',
    fontWeight: 'bold',
  },
});
