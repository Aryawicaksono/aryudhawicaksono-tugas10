// import {View, Text} from 'react-native';
// import React from 'react';
// import {
//   TextRegular,
//   TextMedium,
//   TextBold,
//   Header,
//   InputText,
// } from '../../component/global';
// import {Colors} from '../../styles';
// import {NumberFormatter} from '../../utils/Helpher';

// export default function Login({navigation, route}) {
//   return (
//     <View style={{flex: 1, backgroundColor: '#FFFF'}}>
//       <Header
//         title="Login"
//         onPressBack={() => navigation.goBack()}
//         isBoldTitle={true}
//         titleSize={18}
//         backgroundHeader={Colors.PRIMARY}
//         iconBackColor={Colors.WHITE}
//         titleColor={Colors.WHITE}
//       />
//       <Text>Login Screen</Text>
//       <TextMedium text="ini component text medium" color={Colors.PRIMARY} />
//       <TextBold text="ini component text bold" color={Colors.DEEPORANGE} />
//       <TextRegular
//         text={`${NumberFormatter(1000000, 'Rp.')}`}
//         color={Colors.BLACK}
//         style={{marginTop: 20}}
//       />
//       <InputText
//         style={{width: '90%', alignSelf: 'center', marginTop: 20}}
//         placeholderText="Masukkan Email"
//         keyboardType="email address"
//       />
//       <InputText
//         style={{width: '90%', alignSelf: 'center', marginTop: 20}}
//         placeholderText="Masukkan Nomor Telepon"
//         keyboardType="number-pad"
//       />
//       <InputText
//         style={{width: '90%', alignSelf: 'center', marginTop: 20}}
//         placeholderText="Masukkan password"
//         isPassword={true}
//       />
//     </View>
//   );
// }

//-----------------------------------------------------------------------------------

// import React from 'react';
// import {View} from 'react-native';

// import LoginComponent from '../../component/section/LoginComponent';

// const Login = ({navigation, route}) => {
//   return (
//     <View style={{flex: 1, backgroundColor: '#FFFF'}}>
//       <LoginComponent navigation={navigation} />
//     </View>
//   );
// };
// export default Login;
//-------------------------------------------------------------------------------------------

import React, {useState} from 'react';
import {View} from 'react-native';

import LoginComponent from '../../component/section/LoginComponent';

const Login = ({navigation, route}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [hidePassword, setHidePassword] = useState('');

  const onLogin = () => {};
  return (
    <View style={{flex: 1, backgroundColor: '#FFFF'}}>
      <LoginComponent navigation={navigation} />
    </View>
  );
};
export default Login;
